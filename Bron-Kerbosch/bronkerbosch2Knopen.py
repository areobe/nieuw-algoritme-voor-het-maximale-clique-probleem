# WERKT VOOR ALLE MAX CLIQUES TE VINDEN VOOR 1 KNOOP

'''
Een beetje uitleg over hoe dit werkt:
1) het bronkerbosch hier werkt ook met lijsten maar het begint bij een index 1 ipv 0 dus het eerste element is een lege lijst en
en ander element is +1 gedaan. Dit wordt gedaan in de functie inlezen
2) Het bronKerbosch algoritme heeft geen output de maximale cliques worden opgeslagen in de reporter classe.
Als je dit wilt raadplegen doe je dit door report.cliques
3) je kan de max. cliques ook wegschrijven dit doe je door in het Bronk algoritme na  maximale clique report.wegschrijven te schrijven
( daarom heeft de report klasse een bestand als input nodig)
4) je kan zelf kiezen welke  knoop  dit overloopt dit geeft dus alle max cliques van die  knoop


'''

import graph_maken
import snap
import os
from os.path import expanduser
from reporter import Reporter
import numpy as np
import json
import time

def inlezenBronk(stringVanGraaf):
    FIn = snap.TFIn(stringVanGraaf)
    Graph = snap.TNGraph.Load(FIn)
    FIn.Len()
    NEIGHBORS = graph_maken.alleBuren(Graph)
    nieuw = []
    for rij in NEIGHBORS:
        zijrij = []
        for element in rij:
            element += 1
            zijrij.append(element)
        nieuw.append(zijrij)
    NEIGHBORS = [[]] + nieuw
    return NEIGHBORS



# het bronk algoritme met een begin- een eindknoop
def bronker_bosch1(clique, candidates, excluded, reporter):
    try:
        eersteElement = clique[0]
    except:
        eersteElement = []
    if eersteElement == teOnderzoekenKnooppunt or eersteElement == []:
        reporter.inc_count()
        if not candidates and not excluded:
            if len(clique) >= MIN_SIZE:
                reporter.record(clique)
                reporter.wegschrijven()     # deze lijn is uitgelegd in puntje (3)
            return

        for v in list(candidates):
            new_candidates = candidates.intersection(NEIGHBORS[v])
            new_excluded = excluded.intersection(NEIGHBORS[v])
            bronker_bosch1(clique + [v], new_candidates, new_excluded, reporter)
            candidates.remove(v)
            excluded.add(v)
    else:
        pass

#verwijdert meerde elementen uit een lijst
#@input list
#@input indices
#@output een lijst zonder de opgegeven indices
def multi_delete(list_, indices):
    indexes = sorted(indices, reverse=True)
    for index in indexes:
        del list_[index]
        list_.insert(index,[])
    return list_

# main functie
def main():
    global NEIGHBORS,NODES,knoop1,knoop2,MIN_SIZE,teOnderzoekenKnooppunt
    MIN_SIZE = 1 # kleinste maximale clique
    start = time.clock()
    NEIGHBORS = inlezenBronk('graaf10000 (1)')
    teOnderzoekenKnooppunt = 1                                    #Geeft alle maximale cliques die dit knooppunt bevatten
    teHouden = [v for v in NEIGHBORS[teOnderzoekenKnooppunt]]
    teHouden.append(teOnderzoekenKnooppunt)
    punten = [x for x in range(1,len(NEIGHBORS))]
    verwijderen = [i for i in punten if i not in teHouden]
    multi_delete(NEIGHBORS,verwijderen)
    NODES = set(range(1,len(NEIGHBORS)))
    report = Reporter('bronk','test2knopen.txt') #class om het aantal maximale cliques op te slaan
    bronker_bosch1([],set(NODES),set(),report) #het algoritme laten lopen
    print len(report.cliques)

if __name__ == '__main__':
    main()