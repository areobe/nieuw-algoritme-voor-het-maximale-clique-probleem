import snap
import graph_maken

# Dit bestand kan grafen aanmaken
# Dit gebeurt via de snap-software

nodes = 100
edges = 1000
NEIGHBORS = graph_maken.graafMaken(nodes,edges,'100-1000')

nieuw = []
for rij in NEIGHBORS:
    zijrij = []
    for element in rij:
        element += 1
        zijrij.append(element)
    nieuw.append(zijrij)


NEIGHBORS = [[]] + nieuw
NODES = set(range(1, len(NEIGHBORS)))

