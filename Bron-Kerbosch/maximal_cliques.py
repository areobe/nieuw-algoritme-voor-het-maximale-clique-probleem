# coding: utf-8

from bronker_bosch1 import bronker_bosch1
from bronker_bosch2 import bronker_bosch2
from bronker_bosch3 import bronker_bosch3
from reporter import Reporter
import time
from data import *
import os

funcs = [bronker_bosch1]
    #,bronker_bosch2,bronker_bosch3]
print os.getcwd()
for func in funcs:
    start_time = time.time()
    report = Reporter('## %s' % func.func_doc,'cliquesWegschrijven.txt')
    func([], set(NODES), set(), report)
    print(report.print_report()[0])
    print("--- %s seconds ---" % (time.time() - start_time))
    print(str(report.print_report()[1]) + ' recursieve calls')
    print len(report.cliques)









