# coding: utf-8

import sys
import threading
from reporter import Reporter
import time
import os
import graph_maken
import multiprocessing
import snap


MIN_SIZE = 3

#leest een graaf in
def inlezen(stringVanGraaf):
    FIn = snap.TFIn(stringVanGraaf)
    Graph = snap.TNGraph.Load(FIn)
    FIn.Len()
    NEIGHBORS = graph_maken.alleBuren(Graph)
    nieuw = []
    for rij in NEIGHBORS:
        zijrij = []
        for element in rij:
            element += 1
            zijrij.append(element)
        nieuw.append(zijrij)
    NEIGHBORS = [[]] + nieuw
    return NEIGHBORS

# stopt het programma na een opgegeven tijd
def tijdQuit(tijd):
    time.sleep(tijd)
    print('tijd op,' + ' stop het programma' + ' met os._exit(1)')
    os._exit(1)


def bronker_bosch1(clique, candidates, excluded, reporter):
    reporter.inc_count()
    if not candidates and not excluded:
        if len(clique) >= MIN_SIZE:
            reporter.record(clique)
        return

    for v in list(candidates):
        new_candidates = candidates.intersection(NEIGHBORS[v])
        new_excluded = excluded.intersection(NEIGHBORS[v])
        bronker_bosch1(clique + [v], new_candidates, new_excluded, reporter)
        candidates.remove(v)
        excluded.add(v)
def bronker_bosch2(clique, candidates, excluded, reporter):
    '''Bron–Kerbosch algorithm with pivot'''
    reporter.inc_count()
    if not candidates and not excluded:
        if len(clique) >= MIN_SIZE:
            reporter.record(clique)
            reporter.wegschrijven()
        return

    pivot = pick_random(candidates) or pick_random(excluded)
    for v in list(candidates.difference(NEIGHBORS[pivot])):
        new_candidates = candidates.intersection(NEIGHBORS[v])
        new_excluded = excluded.intersection(NEIGHBORS[v])
        bronker_bosch2(clique + [v], new_candidates, new_excluded, reporter)
        candidates.remove(v)
        excluded.add(v)
def pick_random(s):
    if s:
        elem = s.pop()
        s.add(elem)
        return elem
def bronker_bosch3(clique, candidates, excluded, reporter):
    '''Bron–Kerbosch algorithm with pivot and degeneracy ordering'''
    reporter.inc_count()
    if not candidates and not excluded:
        if len(clique) >= MIN_SIZE:
            reporter.record(clique)
            reporter.wegschrijven()
        return

    for v in list(degeneracy_order(candidates)):
        new_candidates = candidates.intersection(NEIGHBORS[v])
        new_excluded = excluded.intersection(NEIGHBORS[v])
        bronker_bosch2(clique + [v], new_candidates, new_excluded, reporter)
        candidates.remove(v)
        excluded.add(v)
def degeneracy_order(nodes):
    deg = {}
    for node in nodes:
        deg[node] = len(NEIGHBORS[node])

    while deg:
        i, v = min(deg.iteritems(), key=lambda (i, v): v)
        yield i
        del deg[i]
        for v in NEIGHBORS[i]:
            if v in deg:

                deg[v] -= 1

funcs = [bronker_bosch1,bronker_bosch2,bronker_bosch3]
bestand = 'cliquesWegschrijven.txt'
grafenlijst = ['graaf1000 (4)']



def main():
    for graaf in grafenlijst:
        global NEIGHBORS,NODES
        NEIGHBORS = inlezen(graaf)
        NODES = set(range(1,len(NEIGHBORS)))
        versie = int(input('welke versie van het Bron-Kerbosch algoritme 1,2 of 3? '))
        naam = str(funcs[versie - 1].func_name)
        report = Reporter('## %s' % naam,bestand)
        start = time.clock()
        bronker_bosch1([], set(NODES), set(), report)
        einde = time.clock() - start
        with open(bestand,'a') as file:
            file.write(str(report.name))
            file.write('\n')
            file.write(str(report.cnt)+ ' aantal recursieve calls')
            file.write('\n')
            file.write(str(einde))
            file.write('\n')

if __name__ == '__main__':
    main()


