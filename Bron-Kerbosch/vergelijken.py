# coding: utf-8
import snap
import graph_maken
from reporter import Reporter
import time
import os

laagNaarHoogGrafen = ['graaf1000 (4)']

wegschrijfBestand = open('wegschrijf.txt','a')

MIN_SIZE = 3

def bronker_bosch1(clique, candidates, excluded, reporter):
    reporter.inc_count()
    if not candidates and not excluded:
        if len(clique) >= MIN_SIZE:
            reporter.record(clique)
        return

    for v in list(candidates):
        new_candidates = candidates.intersection(NEIGHBORS[v])
        new_excluded = excluded.intersection(NEIGHBORS[v])
        bronker_bosch1(clique + [v], new_candidates, new_excluded, reporter)
        candidates.remove(v)
        excluded.add(v)

def bronker_bosch2(clique, candidates, excluded, reporter):
    reporter.inc_count()
    if not candidates and not excluded:
        if len(clique) >= MIN_SIZE:
            reporter.record(clique)
        return

    pivot = pick_random(candidates) or pick_random(excluded)
    for v in list(candidates.difference(NEIGHBORS[pivot])):
        new_candidates = candidates.intersection(NEIGHBORS[v])
        new_excluded = excluded.intersection(NEIGHBORS[v])
        bronker_bosch2(clique + [v], new_candidates, new_excluded, reporter)
        candidates.remove(v)
        excluded.add(v)

def pick_random(s):
    if s:
        elem = s.pop()
        s.add(elem)
        return elem

def bronker_bosch3(clique, candidates, excluded, reporter):
    reporter.inc_count()
    if not candidates and not excluded:
        if len(clique) >= MIN_SIZE:
            reporter.record(clique)
        return

    for v in list(degeneracy_order(candidates)):
        new_candidates = candidates.intersection(NEIGHBORS[v])
        new_excluded = excluded.intersection(NEIGHBORS[v])
        bronker_bosch2(clique + [v], new_candidates, new_excluded, reporter)
        candidates.remove(v)
        excluded.add(v)

def degeneracy_order(nodes):
    deg = {}
    for node in nodes:
        deg[node] = len(NEIGHBORS[node])

    while deg:
        i, v = min(deg.iteritems(), key=lambda (i, v): v)
        yield i
        del deg[i]
        for v in NEIGHBORS[i]:
            if v in deg:

                deg[v] -= 1

def inlezen(stringVanGraaf):
    FIn = snap.TFIn(stringVanGraaf)
    Graph = snap.TNGraph.Load(FIn)
    FIn.Len()
    NEIGHBORS = graph_maken.alleBuren(Graph)
    nieuw = []
    for rij in NEIGHBORS:
        zijrij = []
        for element in rij:
            element += 1
            zijrij.append(element)
        nieuw.append(zijrij)
    NEIGHBORS = [[]] + nieuw
    return NEIGHBORS

def vergelijken():
    funcs = [bronker_bosch1
        #,bronker_bosch2,bronker_bosch3
        ]
    for func in funcs:
        start_time = time.time()
        report = Reporter('## %s' % func.func_doc)
        func([], set(NODES), set(), report)
        wegschrijfBestand.write(("--- %s seconds ---" % (time.time() - start_time)))
        report = report.print_report()
        wegschrijfBestand.write(str(report[0]))
        wegschrijfBestand.write(' recurieve calls: ' + str(report[1]))
        wegschrijfBestand.write('\n')

def main():
    for i in range(len(laagNaarHoogGrafen)):
        wegschrijfBestand.write('graaf'+ str(i) + ': ' + str(laagNaarHoogGrafen[i]) )
        wegschrijfBestand.write('\n')
        global NEIGHBORS,NODES
        NEIGHBORS = inlezen(i)
        NODES = set(range(1, len(NEIGHBORS)))
        vergelijken()
        wegschrijfBestand.write('\n')
        wegschrijfBestand.write('\n')
        wegschrijfBestand.flush()
        print(i)

wegschrijfBestand.close()

