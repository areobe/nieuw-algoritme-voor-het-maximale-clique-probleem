# README #

### What is this repository for? ###

Dit is de repository voor ons project van P&O3.
Deze repository bevat alle belangrijke code die we hebben gemaakt voor 
het probleem om alle maximale cliques in een graaf te vinden.
Deze repo is bedoeld om onze code toegankelijk te maken voor iedereen en niet om onderhouden te worden.
Deze repo gebruikt een stukje van een andere repo.
https://github.com/cornchz/Bron-Kerbosch

### Who do I talk to? ###

Repo owner 
'robbe.louage@student.kuleuven.be'