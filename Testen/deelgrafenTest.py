from deelgrafen import *
import os
import time
from reporter import Reporter
import multiprocessing
import graph_maken
import snap


grafen = ['graaf1000 (4)']



def main100():
    for i,graaf in enumerate(grafen):
        print(i,graaf)
        looptijd = int(input('geef de looptijd: '))
        buren =  inlezenBronk(graaf)
        print('ingelezen')
        value = multiprocessing.Value('i', 0)
        p = multiprocessing.Process(target=uitvoeren,args=(value,buren))
        p.start()
        print' gestart'
        time.sleep(looptijd)
        with open('wegschrijven.txt','a') as file:
            file.write(str(graaf) + ' seconden: ' + str(looptijd) + ' ' + str(value.value))
            file.write('\n')
        p.terminate()

if __name__ == '__main__':
    main100()