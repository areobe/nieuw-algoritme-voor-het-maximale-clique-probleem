import random
import timeit
import time
from reporter import Reporter
MIN_SIZE = 1

## verdeelInKleurklassen verdeelt de knooppunten van de graaf over kleurklassen. Elke kleurklasse bevat
# knooppunten die onderling geen buren zijn. De opvulling gebeurt vanuit het knooppunt perspectief.
# Dit wil zeggen dat voor elk knooppunt een geschikte kleurklasse wordt gezocht en indien die niet bestaat,
# wordt een nieuwe kleurklasse gemaakt.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# return kleurklassen: een lijst met kleurklassen die elk zijn voorgesteld door een lijst van knooppunten

def verdeelInKleurklassen(graaf):
    start = time.clock()
    kleurklassen = [[1]]
    for i in range(2, len(graaf)):
        for i2 in range(len(kleurklassen)):
            if set(graaf[i]).intersection(set(kleurklassen[i2])) != set():
                if i2 == len(kleurklassen) - 1:
                    kleurklassen.append([i])
            else:
                kleurklassen[i2].append(i)
                break
    print time.clock() - start
    return kleurklassen

## bepaalGewogenKansenKleurklassen bepaalt de kans waarmee elke kleurklasse wordt geselecteerd om later een knooppunt
# uit te kiezen. Deze kans is evenredig met de som van de graden van alle knooppunten binnen de kleurklasse. De kans wordt in een lijst
# voorgesteld door het aantal keren dat het nummer van de kleur erin voorkomt ten op zichte van de totale grootte van de lijst.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklassen: de lijst met de gebruikte kleurklassen van toepassing op de graaf
# return lijstKleurklassen: een lijst met de kleur. Het aantal keer dat elke kleur aanwezig is, is evenredig met zijn graad
 
def bepaalGewogenKansenKleurklassen(graaf,kleurklassen):
    lijstKleurklassen = []
    for i in range(0,len(kleurklassen)):
        for knooppunt in kleurklassen[i]:
            lijstKleurklassen += len(graaf[knooppunt])*[i]
    return lijstKleurklassen

## kiesKleurklasseMetGewogenKansen kiest een kleurklasse op basis van de som van de graden
# van de knooppunten in deze kleurklassen
# lijstKleurklassen: de lijst met de kleurklassen; de aanwezigheid van elke kleurklasse is evenredig met zijn graad
# return kleurklasse: het nummer van de gekozen kleurklasse
 
def kiesKleurklasseMetGewogenKansen(lijstKleurklassen):
    kleurklasse = lijstKleurklassen[random.randint(0,len(lijstKleurklassen)-1)]
    return kleurklasse

## bepaalGewogenKansenKnooppuntenVoorEenKleurklasse bepaalt de kans waarmee een knooppunt gekozen wordt
# uit een gegeven kleurklasse. De kanse is evenredig met het aantal buren van het knooppunt. De kans wordt in een lijst
# voorgesteld door het aantal keren dat het knooppunt erin voorkomt ten op zichte van de totale grootte van de lijst.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklasse: de lijst met de gekozen kleurklasse
# return lijstKnooppuntenKleurklasse: een lijst met knooppunten. Het aantal keer dat elk knooppunt aanwezig is, is evenredig met zijn graad.
 
def bepaalGewogenKansenKnooppuntenVoorEenKleurklasse(graaf,kleurklasse):
    lijstKnooppuntenKleurklasse = []
    for knooppunt in kleurklasse:
        lijstKnooppuntenKleurklasse += len(graaf[knooppunt])*[knooppunt]
    return lijstKnooppuntenKleurklasse

## bepaalGewogenKansenKnooppuntenBinnenElkeKleurklasse bepaalt de kansen voor elk knooppunt binnen elke kleurklasse.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# return lijstKnooppuntenPerKleurklasse: een lijst met per kleurklasse een lijst met de de knooppunten;
# Het aantal keer dat elk knooppunt aanwezig is, is evenredig met zijn graad.
 
def bepaalGewogenKansenKnooppuntenBinnenElkeKleurklasse(graaf,kleurklassen):
    lijstAlleKnooppuntenPerKleurklasse = []
    for kleurklasse in kleurklassen:
        lijstAlleKnooppuntenPerKleurklasse.append(bepaalGewogenKansenKnooppuntenVoorEenKleurklasse(graaf,kleurklasse))
    return lijstAlleKnooppuntenPerKleurklasse

## kiesKnooppuntUitKleurklasse kiest een knooppunt uit een kleurklasse met een kans evenredig aan zijn aantal buren.
# lijstKnooppunten: een lijst met knooppunten. Het aantal keer dat elk knooppunt aanwezig is, is evenredig met zijn graad.
# return knooppunt: het gekozen knooppunt
 
def kiesKnooppuntUitKleurklasse(lijstKnooppuntenKleurklasse):
    knooppunt = lijstKnooppuntenKleurklasse[random.randint(0,len(lijstKnooppuntenKleurklasse)-1)]
    return knooppunt
 
def cliqueTest(verzameling,graaf):
    clique = True
    knoop = 0
    testknoop = 1
    while clique and knoop < len(verzameling)-1:
        if verzameling[testknoop] in graaf[verzameling[knoop]]:
            if testknoop < len(verzameling)-1:
                testknoop += 1
            else:
                knoop += 1
                testknoop = knoop + 1
        else:
            clique = False
    return clique
 
## maximaleCliqueTest gaat na of een gegeven clique maximaal is
# clique: de te controleren clique in de vorm van een lijst
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# return maximaleClique: een boolean die True geeft als de clique maximaal is
 
def maximaleCliqueTest(clique,graaf,kleurklassen):
    if len(clique) == len(kleurklassen):
        maximaleClique = True
    else:
        gemeenschappelijkeBuren = set()
        for knoop in clique:
            if gemeenschappelijkeBuren == set():
                gemeenschappelijkeBuren = set(graaf[knoop])
            else:
                gemeenschappelijkeBuren = gemeenschappelijkeBuren.intersection(set(graaf[knoop]))
        if gemeenschappelijkeBuren == set():
            maximaleClique = True
        else:
            maximaleClique = False
    return maximaleClique
 
## vindNieuwKnooppunt genereert een nieuw knooppunt om aan een clique toe te voegen.
# Dit knooppunt komt uit een andere kleurklasse dan de kleurklassen van de knooppunten in de clique. De kans om een bepaald
# knooppunt te kiezen stijgt met de som van het aantal buren van alle knooppunten in zijn kleurklasse en binnen de kleurklasse
# met zijn eigen aantal buren.
# clique: een lijst die de uit te breiden clique bevat
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# lijstKleurklassen: de lijst met de kleurklassen; de aanwezigheid van elke kleurklasse is evenredig met zijn graad
# lijstAlleKnooppuntenPerKleurklasse: de lijst met per kleurklasse de kans voor elk knooppunt
# return nieuweKnooppunt: het uitgekozen nieuwe knooppunt
 
def vindNieuwKnooppunt(clique,graaf,lijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse,maxAantalIteraties):
    buur = False
    count = 0
    while buur == False and count < maxAantalIteraties:
        kleur = kiesKleurklasseMetGewogenKansen(lijstKleurklassen)
        nieuweKnooppunt = kiesKnooppuntUitKleurklasse(lijstAlleKnooppuntenPerKleurklasse[kleur])
        buur = True
        for knoop in clique:
            if nieuweKnooppunt not in graaf[knoop]:
                buur = False
                break
        count += 1
    if count == maxAantalIteraties:
        nieuweKnooppunt = "Niet gevonden"
    else:
        while kleur in lijstKleurklassen:
            lijstKleurklassen.remove(kleur)
    return nieuweKnooppunt

def bronker_bosch1(value,graaf, clique, candidates, excluded, reporter, aantal = -1):
    reporter.inc_count()
    if not candidates and not excluded:
        if len(clique) >= MIN_SIZE:
            reporter.record(clique)
            value.value += 1
        return
    if aantal == -1:
        aantal = len(candidates)
    for v in list(candidates)[0:aantal]:
        new_candidates = candidates.intersection(graaf[v])
        new_excluded = excluded.intersection(graaf[v])
        bronker_bosch1(value,graaf, clique + [v], new_candidates, new_excluded, reporter)
        candidates.remove(v)
        excluded.add(v)

def bronker_boschAangepast(value,graaf, clique, candidates, excluded, reporter, extraMaximaleCliques, aantal = -1):

    reporter.inc_count()
    if not candidates and not excluded:
        reporter.record(clique)
        value.value += 1
        return
    if aantal == -1:
        aantal = len(candidates)
    for v in list(candidates)[0:aantal]:
        new_candidates = candidates.intersection(graaf[v])
        new_excluded = excluded.intersection(graaf[v])
        doorgaan = True
        if new_candidates.union(set(clique + [v])) in extraMaximaleCliques:
            doorgaan = False
            break
        if doorgaan == True:
            bronker_boschAangepast(value,graaf,clique + [v], new_candidates, new_excluded, reporter, extraMaximaleCliques)
        candidates.remove(v)
        excluded.add(v)

def verwijderKleineKnooppuntenUitMaximaleCliques(maximaleCliques,eersteKnoop,laatsteKnoop,minimumGrootte = 2,vorigeCliques = []):
    lijstVanCliques = []
    nieuweClique = set()
    for maximaleClique in maximaleCliques:
        for i in range(eersteKnoop,laatsteKnoop+1):
            if i in maximaleClique:
                if nieuweClique == set():
                    nieuweClique = set(maximaleClique).difference({i})
                else:
                    nieuweClique = nieuweClique.difference({i})
        if not len(nieuweClique) < minimumGrootte:
            reedsGezien = False
            for clique in vorigeCliques:
                if clique == nieuweClique:
                    reedsGezien = True
                    break
            if reedsGezien == False:
                if lijstVanCliques == []:
                    lijstVanCliques.append(nieuweClique)
                else:
                    gevonden = False
                    for clique in lijstVanCliques:
                        if clique == nieuweClique:
                            gevonden = True
                    if gevonden == False:
                        lijstVanCliques.append(nieuweClique)
    return lijstVanCliques

def vindKleuren(verzameling,kleurklassen):
    kleuren = set()
    for element in verzameling:
        for kleur in range(len(kleurklassen)):
            if element in kleurklassen[kleur]:
                kleuren.add(kleur)
                break
    return kleuren

def nieuweLijstKleurklassenMaken(clique,kleurklassen,lijstKleurklassen):
    kleuren = vindKleuren(clique,kleurklassen)
    nieuweLijstKleurklassen = list(lijstKleurklassen)
    for kleur in kleuren:
        while kleur in nieuweLijstKleurklassen:
            nieuweLijstKleurklassen.remove(kleur)
    return nieuweLijstKleurklassen

def knooppuntUitLijstenVerwijderen(knooppunt,lijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse):
    for kleur in range(len(lijstAlleKnooppuntenPerKleurklasse)):
        if knooppunt in lijstAlleKnooppuntenPerKleurklasse[kleur]:
            i = 0
            while knooppunt in lijstAlleKnooppuntenPerKleurklasse[kleur]:
                lijstAlleKnooppuntenPerKleurklasse[kleur].remove(knooppunt)
                i += 1
            for aantal in range(i):
                lijstKleurklassen.remove(kleur)
            break
    return

def probeerCliquesUitTeBreiden(value,report,cliques,graaf,kleurklassen,lijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse,aantalKeerZoeken,maxPogingenNieuwKnooppunt,extraMaxCliquesOud = []):
    extraMaximaleCliques = []
    for clique in cliques:
        for i in range(aantalKeerZoeken):
            nieuweClique = set(clique)
            nieuweLijstKleurklassen = nieuweLijstKleurklassenMaken(clique,kleurklassen,lijstKleurklassen)
            if nieuweLijstKleurklassen == []:
                break
            else:
                nieuweKnoop = vindNieuwKnooppunt(nieuweClique,graaf,nieuweLijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse,maxPogingenNieuwKnooppunt)
                einde = (nieuweKnoop == "Niet gevonden")
                if einde == False:
                    nieuweClique.add(nieuweKnoop)
                maximaal = maximaleCliqueTest(nieuweClique,graaf,kleurklassen)
                while maximaal == False and einde == False:
                    nieuweKnoop = vindNieuwKnooppunt(nieuweClique,graaf,nieuweLijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse,maxPogingenNieuwKnooppunt)
                    einde = (nieuweKnoop == "Niet gevonden")
                    if einde == False:
                        nieuweClique.add(nieuweKnoop)
                        maximaal = maximaleCliqueTest(nieuweClique,graaf,kleurklassen)
                if maximaal == True:
                    gevonden = False
                    if nieuweClique in extraMaxCliquesOud:
                        gevonden = True
                        break
                    if nieuweClique in extraMaximaleCliques:
                        gevonden = True
                        break
                    if gevonden == False:
                        extraMaximaleCliques.append(nieuweClique)
                        report.record(nieuweClique)
                        value.value += 1
    return extraMaximaleCliques

def maximaleCliques(graaf,aantalKnopenPerIteratie,aantalKeerZoeken,maxPogingenNieuwKnooppunt,report,value):
    NODES = set(range(1,len(graaf)))
    kleurklassen = verdeelInKleurklassen(graaf)
    lijstKleurklassen = bepaalGewogenKansenKleurklassen(graaf,kleurklassen)
    lijstAlleKnooppuntenPerKleurklasse = bepaalGewogenKansenKnooppuntenBinnenElkeKleurklasse(graaf,kleurklassen)
    bronker_bosch1(value,graaf,[],NODES,set(),report,aantalKnopenPerIteratie)
    maximaleCliques = report.cliques
    for knoop in range(1,aantalKnopenPerIteratie+1):
        knooppuntUitLijstenVerwijderen(knoop,lijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse)
    cliques = verwijderKleineKnooppuntenUitMaximaleCliques(maximaleCliques,1,aantalKnopenPerIteratie,2)
    extraMaximaleCliques = probeerCliquesUitTeBreiden(value,report,cliques,graaf,kleurklassen,lijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse,aantalKeerZoeken,maxPogingenNieuwKnooppunt)
    for i in range(2,(len(graaf)-1)//aantalKnopenPerIteratie+1):
        if i == len(graaf)//aantalKnopenPerIteratie:
            aantalKnopenPerIteratie = aantalKnopenPerIteratie + (len(graaf)-1)%aantalKnopenPerIteratie
        report = Reporter("bronkerbosch",'michiel1m.txt')
        bronker_boschAangepast(value,graaf,[],set(range((i-1)*aantalKnopenPerIteratie+1,len(graaf))),set(range(1,(i-1)*aantalKnopenPerIteratie+1)),report,extraMaximaleCliques,aantalKnopenPerIteratie)
        maximaleCliques += report.cliques
        for knoop in range((i-1)*aantalKnopenPerIteratie+1,(i-1)*aantalKnopenPerIteratie+aantalKnopenPerIteratie+1):
            knooppuntUitLijstenVerwijderen(knoop,lijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse)
        nieuweCliques = verwijderKleineKnooppuntenUitMaximaleCliques(report.cliques,(i-1)*aantalKnopenPerIteratie+1,(i-1)*aantalKnopenPerIteratie+aantalKnopenPerIteratie,2,cliques)
        cliques += nieuweCliques
        extraMaximaleCliques += probeerCliquesUitTeBreiden(value,report,nieuweCliques,graaf,kleurklassen,lijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse,aantalKeerZoeken,maxPogingenNieuwKnooppunt,extraMaximaleCliques)
    for cl in range(len(extraMaximaleCliques)):
        extraMaximaleCliques[cl] = list(extraMaximaleCliques[cl])
    totaalMaximaleCliques = maximaleCliques + extraMaximaleCliques
    return totaalMaximaleCliques
 
