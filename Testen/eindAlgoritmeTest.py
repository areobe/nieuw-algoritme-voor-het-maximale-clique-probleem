import multiprocessing
from eindAlgoritme import *
import snap
import os
import time
import graph_maken
from reporter import Reporter

def inlezenBronk(stringVanGraaf):
    FIn = snap.TFIn(stringVanGraaf)
    Graph = snap.TNGraph.Load(FIn)
    FIn.Len()
    NEIGHBORS = graph_maken.alleBuren(Graph)
    nieuw = []
    for rij in NEIGHBORS:
        zijrij = []
        for element in rij:
            element += 1
            zijrij.append(element)
        nieuw.append(zijrij)
    NEIGHBORS = [[]] + nieuw
    return NEIGHBORS
grafen = ['graaf1000 (4)']

if __name__ == '__main__':
    for i,graaf in enumerate(grafen):
        looptijd = int(input('geef de looptijd'))
        NEIGHBORS = inlezenBronk(graaf)
        print 'ingelezen'
        report = Reporter('michiel', 'test')
        value = multiprocessing.Value('i',0)
        parameter1 = 10
        parameter2 =  1
        parameter3 =  1
        p = multiprocessing.Process(target=maximaleCliques,args=(NEIGHBORS,parameter1,parameter2,parameter3,report,value))
        p.start()
        time.sleep(looptijd)
        with open('wegscchrijven.txt','a') as file:
            file.write(str(graaf) + ' seconden: ' + str(looptijd) + ' ' + str(value.value))
            file.write('\n')
            file.flush()
        p.terminate()
