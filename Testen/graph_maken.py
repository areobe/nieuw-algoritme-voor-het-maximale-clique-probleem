# Dit script is bedoeld om standaard test-graphen te maken zodat de verschillende algoritmes getest kunnen worden.
import snap
import os

# maakt van een snapgraaf een geneste lijst
# indices zijn knopen
# geneste lijsten zijn de buren
def alleBuren(graaf):
    iterator = graaf.BegNI()
    buren = []
    for i in range(graaf.GetNodes()):
        burenVanEenElement = []
        id = iterator.GetId()
        graad = iterator.GetDeg()
        for j in range(graad/2):
            try:
                burenVanEenElement.append(iterator.GetNbrNId(j))
            except RuntimeError as e:
                print(e, 'niet goed eh')
        iterator.Next()
        buren.append(burenVanEenElement)
    return buren

## maakt een graaf met opgegeven aantal
## nodes en edges en een bestandsnaam
def graafMaken(nodes,edges,bestandsnaam):
    graaf = snap.GenRndGnm(snap.PNGraph, nodes,edges,False)
    #file = "test.txt"
    #snap.SaveEdgeList(graaf, file, "Save as tab-separated list of edges")
    FOut = snap.TFOut(bestandsnaam)
    graaf.Save(FOut)
    FOut.Flush()
    FIn = snap.TFIn(bestandsnaam)
    Graph = snap.TNGraph.Load(FIn)
    FIn.Len()
    buren = alleBuren(Graph)
    return buren



