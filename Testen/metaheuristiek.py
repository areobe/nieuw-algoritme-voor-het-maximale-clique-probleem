import random
import numpy
import timeit

## verdeelInKleurklassen verdeelt de knooppunten van de graaf over kleurklassen. Elke kleurklasse bevat
# knooppunten die onderling geen buren zijn. De opvulling gebeurt vanuit het knooppunt perspectief.
# Dit wil zeggen dat voor elk knooppunt een geschikte kleurklasse wordt gezocht en indien die niet bestaat,
# wordt een nieuwe kleurklasse gemaakt.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# return kleurklassen: een lijst met kleurklassen die elk zijn voorgesteld door een lijst van knooppunten


def verdeelInKleurklassen(graaf):
    kleurklassen = [[1]]
    for i in range(2,len(graaf)):
        for i2 in range(len(kleurklassen)):
            if set(graaf[i]).intersection(set(kleurklassen[i2])) != set():
                if i2 == len(kleurklassen) - 1:
                    kleurklassen.append([i])
                    break
            else:
                kleurklassen[i2].append(i)
                break
    return kleurklassen


## bepaalGewogenKansenKleurklassen bepaalt de kans waarmee elke kleurklasse wordt geselecteerd om later een knooppunt
# uit te kiezen. Deze kans is evenredig met de som van de graden van alle knooppunten binnen de kleurklasse. De kans wordt in een lijst
# voorgesteld door het aantal keren dat het nummer van de kleur erin voorkomt ten op zichte van de totale grootte van de lijst.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklassen: de lijst met de gebruikte kleurklassen van toepassing op de graaf
# return lijstKleurklassen: een lijst met de kleur. Het aantal keer dat elke kleur aanwezig is, is evenredig met zijn graad

def bepaalGewogenKansenKleurklassen(graaf,kleurklassen):
    lijstKleurklassen = []
    for i in range(0,len(kleurklassen)):
        for knooppunt in kleurklassen[i]:
            lijstKleurklassen += len(graaf[knooppunt])*[i]
    return lijstKleurklassen

## kiesKleurklasseMetGewogenKansen kiest een kleurklasse op basis van de som van de graden
# van de knooppunten in deze kleurklassen
# lijstKleurklassen: de lijst met de kleurklassen; de aanwezigheid van elke kleurklasse is evenredig met zijn graad
# return kleurklasse: het nummer van de gekozen kleurklasse

def kiesKleurklasseMetGewogenKansen(lijstKleurklassen):
    kleurklasse = lijstKleurklassen[random.randint(0,len(lijstKleurklassen)-1)]
    return kleurklasse

## bepaalGewogenKansenKnooppuntenVoorEenKleurklasse bepaalt de kans waarmee een knooppunt gekozen wordt
# uit een gegeven kleurklasse. De kanse is evenredig met het aantal buren van het knooppunt. De kans wordt in een lijst
# voorgesteld door het aantal keren dat het knooppunt erin voorkomt ten op zichte van de totale grootte van de lijst.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklasse: de lijst met de gekozen kleurklasse
# return lijstKnooppuntenKleurklasse: een lijst met knooppunten. Het aantal keer dat elk knooppunt aanwezig is, is evenredig met zijn graad.

def bepaalGewogenKansenKnooppuntenVoorEenKleurklasse(graaf,kleurklasse):
    lijstKnooppuntenKleurklasse = []
    for knooppunt in kleurklasse:
        lijstKnooppuntenKleurklasse += len(graaf[knooppunt])*[knooppunt]
    return lijstKnooppuntenKleurklasse

## bepaalGewogenKansenKnooppuntenBinnenElkeKleurklasse bepaalt de kansen voor elk knooppunt binnen elke kleurklasse.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# return lijstKnooppuntenPerKleurklasse: een lijst met per kleurklasse een lijst met de de knooppunten;
# Het aantal keer dat elk knooppunt aanwezig is, is evenredig met zijn graad.

def bepaalGewogenKansenKnooppuntenBinnenElkeKleurklasse(graaf,kleurklassen):
    lijstAlleKnooppuntenPerKleurklasse = []
    for kleurklasse in kleurklassen:
        lijstAlleKnooppuntenPerKleurklasse.append(bepaalGewogenKansenKnooppuntenVoorEenKleurklasse(graaf,kleurklasse))
    return lijstAlleKnooppuntenPerKleurklasse

## kiesKnooppuntUitKleurklasse kiest een knooppunt uit een kleurklasse met een kans evenredig aan zijn aantal buren.
# lijstKnooppunten: een lijst met knooppunten. Het aantal keer dat elk knooppunt aanwezig is, is evenredig met zijn graad.
# return knooppunt: het gekozen knooppunt

def kiesKnooppuntUitKleurklasse(lijstKnooppuntenKleurklasse):
    knooppunt = lijstKnooppuntenKleurklasse[random.randint(0,len(lijstKnooppuntenKleurklasse)-1)]
    return knooppunt

## genereerEersteOplossing bepaalt de eerste mogelijke oplossing.
# aantalVerzamelingen: het gewenste aantal mogelijk cliques in de startoplossing
# lijstAlleKnooppuntenPerKleurklasse: de lijst met per kleurklasse de kans voor elk knooppunt
# return mogelijkeCliques: de eerste oplossing bestaande uit een lijst van lijsten van knooppunten


def genereerEersteOplossing(aantalVerzamelingen,lijstAlleKnooppuntenPerKleurklasse):
    mogelijkeCliques = []
    for i in range(0,aantalVerzamelingen):
        for kleur in range(0,len(lijstAlleKnooppuntenPerKleurklasse)):
            if kleur == 0:
                mogelijkeCliques.append([kiesKnooppuntUitKleurklasse(lijstAlleKnooppuntenPerKleurklasse[kleur])])
            else:
                mogelijkeCliques[i].append(kiesKnooppuntUitKleurklasse(lijstAlleKnooppuntenPerKleurklasse[kleur]))
    return mogelijkeCliques

## cliqueTest gaat na of een gegeven verzameling van knooppunten een clique is
# verzameling: een lijst met de te controleren verzameling van knooppunten
# graaf: de te onderzoeken graaf (lijst van lijsten)
# return clique: een boolean die True geeft als de verzameling een clique vormt

def cliqueTest(verzameling,graaf):
    clique = True
    knoop = 0
    testknoop = 1
    while clique and knoop < len(verzameling)-1:
        if verzameling[testknoop] in graaf[verzameling[knoop]]:
            if testknoop < len(verzameling)-1:
                testknoop += 1
            else:
                knoop += 1
                testknoop = knoop + 1
        else:
            clique = False
    return clique

## maximaleCliqueTest gaat na of een gegeven clique maximaal is
# clique: de te controleren clique in de vorm van een lijst
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# return maximaleClique: een boolean die True geeft als de clique maximaal is

def maximaleCliqueTest(clique,graaf,kleurklassen):
    if len(clique) == len(kleurklassen):
        maximaleClique = True
    else:
        gemeenschappelijkeBuren = set(graaf[clique[0]])
        for i in range(1,len(clique)):
            gemeenschappelijkeBuren = gemeenschappelijkeBuren.intersection(set(graaf[clique[i]]))
        if gemeenschappelijkeBuren == set():
            maximaleClique = True
        else:
            maximaleClique = False
    return maximaleClique

## nieuweKandidaatClique genereert een nieuwe mogelijke clique met uit elke kleurklasse een knooppunt
# lijstAlleKnooppuntenPerKleurklasse: de lijst met per kleurklasse de kans voor elk knooppunt
# return nieuweMogelijkeClique: een lijst met een nieuwe verzameling van knooppunten

def nieuweKandidaatClique(lijstAlleKnooppuntenPerKleurklasse):
    nieuweMogelijkeClique = []
    for kleur in range(0,len(lijstAlleKnooppuntenPerKleurklasse)):
            nieuweMogelijkeClique.append(kiesKnooppuntUitKleurklasse(lijstAlleKnooppuntenPerKleurklasse[kleur]))
    return nieuweMogelijkeClique

## vindNieuwKnooppunt genereert een nieuw knooppunt om aan een clique toe te voegen.
# Dit knooppunt komt uit een andere kleurklasse dan de kleurklassen van de knooppunten in de clique. De kans om een bepaald
# knooppunt te kiezen stijgt met de som van het aantal buren van alle knooppunten in zijn kleurklasse en binnen de kleurklasse
# met zijn eigen aantal buren.
# clique: een lijst die de uit te breiden clique bevat
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# lijstKleurklassen: de lijst met de kleurklassen; de aanwezigheid van elke kleurklasse is evenredig met zijn graad
# lijstAlleKnooppuntenPerKleurklasse: de lijst met per kleurklasse de kans voor elk knooppunt
# return nieuweKnooppunt: het uitgekozen nieuwe knooppunt

def vindNieuwKnooppunt(clique,kleurklassen,lijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse):
    kleur = kiesKleurklasseMetGewogenKansen(lijstKleurklassen)
    goedgekeurd = set(kleurklassen[kleur]).intersection(set(clique)) == set()
    while not goedgekeurd:
        kleur = kiesKleurklasseMetGewogenKansen(lijstKleurklassen)
        goedgekeurd = set(kleurklassen[kleur]).intersection(set(clique)) == set()
    nieuweKnooppunt = kiesKnooppuntUitKleurklasse(lijstAlleKnooppuntenPerKleurklasse[kleur])
    return nieuweKnooppunt


## genereerMaximaleCliques genereert een bepaald aantal maximale cliques in een graaf
# return maximaleCliques: een lijst van gevonde maximale cliques die zelf worden voorgesteld door lijsten


def genereerMaximaleCliques(value,graaf,maxAantalIteraties,initieelAantalKandidaatCliques):
    kleurklassen = verdeelInKleurklassen(graaf)
    lijstKleurklassen = bepaalGewogenKansenKleurklassen(graaf,kleurklassen)
    lijstAlleKnooppuntenPerKleurklasse = bepaalGewogenKansenKnooppuntenBinnenElkeKleurklasse(graaf,kleurklassen)
    huidigeOplossing = genereerEersteOplossing(initieelAantalKandidaatCliques,lijstAlleKnooppuntenPerKleurklasse)
    besteOplossing = huidigeOplossing
    maximaleCliques = []
    nieuweOplossing = huidigeOplossing
    n = 0
    tijd0 = 0
    tijd1 = 0
    tijd2 = 0
    tijd3 = 0
    a0 = 0
    a1 = 0

    while n < maxAantalIteraties:
        aantalKandidaatCliques = len(huidigeOplossing)
        gekozenKandidaatClique = random.randint(0,aantalKandidaatCliques-1)
        start0 = timeit.default_timer()
        isClique = cliqueTest(nieuweOplossing[gekozenKandidaatClique],graaf)
        stop0 = timeit.default_timer()
        tijd0 += (stop0 - start0)
        if isClique != True:
            teVerwijderenKnoop = random.randint(0,len(nieuweOplossing[gekozenKandidaatClique])-1)
            nieuweOplossing[gekozenKandidaatClique].pop(teVerwijderenKnoop)
        else:
            start1 = timeit.default_timer()
            isCliqueMaximaal = maximaleCliqueTest(huidigeOplossing[gekozenKandidaatClique],graaf,kleurklassen)
            stop1 = timeit.default_timer()
            tijd1 += (stop1 - start1)
            if isCliqueMaximaal == True:
                maximaleClique = nieuweOplossing.pop(gekozenKandidaatClique)
                if maximaleClique not in maximaleCliques:
                    maximaleCliques.append(maximaleClique)
                    value.value += 1
                start2 = timeit.default_timer()
                nieuweOplossing.append(nieuweKandidaatClique(lijstAlleKnooppuntenPerKleurklasse))
                stop2 = timeit.default_timer()
                tijd2 += (stop2 - start2)
                a0 += 1
            else:
                start3 = timeit.default_timer()
                nieuweKnooppunt = vindNieuwKnooppunt(huidigeOplossing[gekozenKandidaatClique],kleurklassen,lijstKleurklassen,lijstAlleKnooppuntenPerKleurklasse)
                nieuweOplossing[gekozenKandidaatClique].append(nieuweKnooppunt)
                stop3 = timeit.default_timer()
                tijd3 += (stop3 - start3)
                a1 += 1
        n += 1
    return maximaleCliques












