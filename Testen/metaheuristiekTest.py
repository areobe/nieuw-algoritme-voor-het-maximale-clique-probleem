from metaheuristiek import *
import os
import time
from reporter import Reporter
import multiprocessing
import graph_maken
import snap


grafen = [ 'graaf1000 (4)']


def main1000():
    for i,graaf in enumerate(grafen):
        looptijd = int(input('geef de looptijd'))
        with open(graaf + '.txt','r') as file:
            buren = json.load(file)
        print(graaf)
        report = Reporter('michiel', 'test')
        value = multiprocessing.Value('i',0)
        maxAantalIteraties = int(input('geef maximaal aantal iteraties: '))
        initieelAantalKandidaatCliques = 12
        p = multiprocessing.Process(target=genereerMaximaleCliques,args=(value,buren,maxAantalIteraties,initieelAantalKandidaatCliques))
        start = time.clock()
        p.start()
        time.sleep(looptijd)
        with open('wegschrijven.txt','a') as file:
            file.write(str(graaf) + ' seconden: ' + str(looptijd) + ' ' + str(value.value))
            file.write('\n')
        p.terminate()

if __name__ == '__main__':
    main1000()

