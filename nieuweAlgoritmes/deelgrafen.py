#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division


from copy import copy
import snap
import os
import graph_maken
from reporter import Reporter


def bronker_bosch1(clique, candidates, excluded, reporter):
    reporter.inc_count()
    if not candidates and not excluded:
        if len(clique) >= MIN_SIZE:
            reporter.record(clique)
        return

    for v in list(candidates):
        new_candidates = candidates.intersection(NEIGHBORS[v])
        new_excluded = excluded.intersection(NEIGHBORS[v])
        bronker_bosch1(clique + [v], new_candidates, new_excluded, reporter)
        candidates.remove(v)
        excluded.add(v)


#Het idee is om de graaf willekeurig in deelgrafen te verdelen, en dan voor elke knoop kijken in welke deelgraaf hij het
# meeste buren heeft, en hem dan te verplaatsen naar deze graaf.
def GenereerDeelgrafen(graaf,aantalDeelgrafen):
    lengteGraaf = len(graaf) #Graaf is al aangepast om BK te doen
    lengteDeelgraaf = int(lengteGraaf/aantalDeelgrafen)
    deelgrafen = [[] for q in range(0,aantalDeelgrafen)]
    for i in range(0,aantalDeelgrafen):
        deelgraaf = deelgrafen[i]
        for j in range(0,lengteDeelgraaf):
            deelgraaf.append(j+i*lengteDeelgraaf+1)
    return deelgrafen

#Deze functie verplaatst alle knopen naar de deelgraaf waar ze het meeste buren hebben

def VerplaatsKnopen(graaf,deelgrafen):
    for i in range(0,len(deelgrafen)):
        deelgraaf = copy(deelgrafen[i])
        lengte = len(deelgraaf)
        for j in range(0,lengte):
            knoop = deelgraaf[j]
            buren = graaf[knoop]
            aantalBurenPerDeelgraaf = [0]*len(deelgrafen)
            for element in buren:
                positie = ZoekInWelkeDeelgraafKnoopZit(deelgrafen,element)
                aantalBurenPerDeelgraaf[positie] += 1
            maximumIndex = aantalBurenPerDeelgraaf.index(max(aantalBurenPerDeelgraaf))
            gemiddeldeLengte = len(graaf)/len(deelgrafen)
            if len(deelgrafen[maximumIndex]) < 1.10*gemiddeldeLengte and len(deelgrafen[i]) > 0.9*gemiddeldeLengte:
                deelgrafen[i].remove(knoop)
                deelgrafen[maximumIndex].append(knoop)
    return deelgrafen

#Dit is een hulpfunctie voor de vorige functie

def ZoekInWelkeDeelgraafKnoopZit(deelgrafen,knoop):
    for i in range(0,len(deelgrafen)):
        deelgraaf = deelgrafen[i]
        if knoop in deelgraaf:
            positie = i
    return positie



#In het ideale geval kunnen we een subgraaf van een graaf afscheiden zonder edges te verliezen. Alle cliques die dan
#maximaal zijn in de subgraaf, zijn dit ook in de volledige graaf. Deze functie bekijkt hoeveel edges er per knoop
#verloren gaan bij het afscheiden van de subgraaf. verlorenEdges een lijst, waarbij het element op positie k voorstelt
#hoeveel edges het element op positie k in subgraaf verloren heeft.

def TelVerlorenEdges(subgraaf,graaf):
    verlorenEdges = []
    for i in range(0,len(subgraaf)):
        buren = graaf[subgraaf[i]]
        teller = 0
        for knoop in buren:
            if knoop not in subgraaf:
                teller += 1
        verlorenEdges.append(teller)
    return verlorenEdges


#Tel het aantal edges dat de subgraaf bevat

def TelTotaalAantalEdges(graaf,subgraaf):
    AantalEdges = 0
    for i in range(0,len(subgraaf)):
        knoop = subgraaf[i]
        lengte = len(graaf[knoop])
        AantalEdges += lengte
    return AantalEdges

#Tel het aantal edges dat verloren gaat

def TelTotaalAantalVerlorenEdges(verlorenEdges):
    AantalVerlorenEdges = 0
    for i in range(0,len(verlorenEdges)):
        AantalVerlorenEdges += verlorenEdges[i]
    return AantalVerlorenEdges

#Deze functie geeft aan of een subgraaf voldoet aan bepaalde voorwaarden, maar wordt niet gebruikt in het algoritme
def BeoordeelSubgraaf(AantalEdges,AantalVerlorenEdges,CriteriumWaarde):
    verhouding = AantalVerlorenEdges/AantalEdges
    print(verhouding)
    if verhouding < CriteriumWaarde:
        return True
    else:
        return False

#De lijst met knopen uit de deelgraaf moet nu worden omgezet in een lijst van lijsten (een graaf), waar BK op kan toegepast
#worden.

def ZetDeelgraafOm(graaf,deelgraaf):
    nieuweGraaf = [[] for i in range(0,len(graaf))]
    for j in range(0,len(graaf)):
        if j in deelgraaf:
            nieuweGraaf[j] = graaf[j]
        else:
            nieuweGraaf[j] = []
    return nieuweGraaf

#Ieder knooppunt wordt voorgesteld door zijn buren, maar de buren die niet in de deelgraaf zitten moeten hieruit dus
#nog worden verwijderd.


def VerwijderOverbodigeKnopenUitDeelgraaf(deelgraaf):
    for i in range(0,len(deelgraaf)):
        buren = deelgraaf[i]
        if buren != []:
            for element in buren:
                if deelgraaf[element] == []:
                    buren.remove(element)
        deelgraaf[i] = buren
    return deelgraaf



#Syntax van Bron Kerbosch


def alleBuren(graaf):
    iterator = graaf.BegNI()
    buren = []
    for i in range(graaf.GetNodes()):
        burenVanEenElement = []
        id = iterator.GetId()
        graad = iterator.GetDeg()
        for j in range(int(graad/2)):
            try:
                burenVanEenElement.append(iterator.GetNbrNId(j))
            except RuntimeError as e:
                print(e, 'niet goed eh')
        iterator.Next()
        buren.append(burenVanEenElement)
    return buren

def inlezen(stringVanGraaf):
    FIn = snap.TFIn(stringVanGraaf)
    Graph = snap.TNGraph.Load(FIn)
    FIn.Len()
    NEIGHBORS = alleBuren(Graph)
    return NEIGHBORS

def inlezenBronk(stringVanGraaf):
    FIn = snap.TFIn(stringVanGraaf)
    Graph = snap.TNGraph.Load(FIn)
    FIn.Len()
    NEIGHBORS = graph_maken.alleBuren(Graph)
    nieuw = []
    for rij in NEIGHBORS:
        zijrij = []
        for element in rij:
            element += 1
            zijrij.append(element)
        nieuw.append(zijrij)
    NEIGHBORS = [[]] + nieuw
    return NEIGHBORS



#Het algoritme bestaat uit 3 delen: het eerste deel staat beschreven in het programma 'deelgrafen'.
#Het tweede deel bestaat eruit Bron Kerbosch toe te passen op elk van die deelgrafen
#Het derde deel, dit programma, gaat de uitkomsten van BK analyseren en daarop verder gaan.

#We hebben de graaf op een relatief efficiënte manier (ongeveer 50% van de edges gaan verloren) in deelgrafen verdeeld.
#Het idee is om te kijken naar alle maximale cliques die meer dan 3(kan nog veranderen) knopen bevatten, en deze eventueel uit te breiden
#met knopen uit andere deelgrafen. Om dan de rest van de maximale cliques ook te vinden, zullen we BronKerbosch laten zoeken naar
#alle maximale cliques met 4 of minder knopen (dit getal kan ook nog varieren).


#Alle maximale cliques van meer dan 3 elementen breiden we nu uit door de doorsnede van de buren te nemen.
#De output is voor iedere gevonden clique, die clique zelf, samen met de doorsnede van de buren

def VindDoorsnedesBuren(MaxCliques,graaf):
    #teVerwijderenIndexen = []
    nieuweMaxCliques = []
    for i in range(0,len(MaxCliques)):
        clique = MaxCliques[i]
        element = clique[0]
        doorsnede1 = graaf[element]
        for j in range(1,len(clique)):
            doorsnede0 = doorsnede1
            element = clique[j]
            buren = graaf[element]
            doorsnede1 = list(set(buren) & set(doorsnede0))
        if doorsnede1 != []:
            clique3 = clique + (doorsnede1)
            #for clique2 in nieuweMaxCliques:
             #   if set(clique3) == set(clique2):
              #      teVerwijderenIndexen.append(i)
            nieuweMaxCliques.append(clique3)
        else:
            nieuweMaxCliques.append(clique)
    #for index in teVerwijderenIndexen:
     #   nieuweMaxCliques.remove(index)
    return nieuweMaxCliques

#Om dan binnen die doorsnede de maximale cliques te vinden, passen we opnieuw BronKerbosch toe.
#We moeten de verzameling knopen dus weer omzetten naar iets waarop we BK kunnen toepassen
def ZetDoorsnedeOmInGraaf(doorsnede,graaf):
    nieuweGraaf = [[] for q in range(0,len(graaf))]
    for i in range(0,len(doorsnede)):
        knoop = doorsnede[i]
        nieuweGraaf[knoop] = graaf[knoop]
    return nieuweGraaf


def VerwijderOverbodigeKnopenUitNieuweGraaf(nieuweGraaf):
    for i in range(0,len(nieuweGraaf)):
        buren = nieuweGraaf[i]
        buren2 = copy(buren)
        if buren != []:
            for element in buren:
                if nieuweGraaf[element] == []:
                    buren2.remove(element)
        nieuweGraaf[i] = buren2
    return nieuweGraaf



def main():
    graaf = inlezenBronk('graaf10000 (2)')
    deelgrafen = GenereerDeelgrafen(graaf,4)
    print(deelgrafen)
    for i in range(0,5):
        #Na de 5e iteratie veranderen de deelgrafen bijna niet meer
        deelgrafen = VerplaatsKnopen(graaf,deelgrafen)
        print(deelgrafen)
    for i in range(0,len(deelgrafen)):
        deelgraaf = deelgrafen[i]
        verlorenEdges = TelVerlorenEdges(deelgraaf,graaf)
        TotaalAantalEdges = (TelTotaalAantalEdges(graaf,deelgraaf))
        AantalVerlorenEdges = (TelTotaalAantalVerlorenEdges(verlorenEdges))
        verhouding = (AantalVerlorenEdges/TotaalAantalEdges)
        print(verhouding)
    maxCliques = []
    for i in range(0,len(deelgrafen)):
        deelgraaf = deelgrafen[i]
        nieuweGraaf = ZetDeelgraafOm(graaf,deelgraaf)
        AangepasteGraaf = VerwijderOverbodigeKnopenUitDeelgraaf(nieuweGraaf)
        report = Reporter('bronk','test.txt')
        global NEIGHBORS, MIN_SIZE
        MIN_SIZE = 3
        NEIGHBORS = AangepasteGraaf
        bronker_bosch1([],set(range(1,(len(NEIGHBORS)))),set(),report)
        maximaleCliques = report.cliques
        maxCliques += (maximaleCliques)
        print "stop"
    print "test"
    print len(maxCliques)
    print maxCliques[0:20]
    UitgebreideMaxCliques = VindDoorsnedesBuren(maxCliques,graaf)
    print UitgebreideMaxCliques[0:20]
    print "test2"
    print len(UitgebreideMaxCliques)
    DefinitieveMaximaleCliques = []
    for clique in UitgebreideMaxCliques:
        nieuweGraaf = ZetDoorsnedeOmInGraaf(clique,graaf)
        nodigeGraaf = VerwijderOverbodigeKnopenUitNieuweGraaf(nieuweGraaf)
        report = Reporter('bronk','test.txt')
        MIN_SIZE = 2
        NEIGHBORS = nodigeGraaf
        bronker_bosch1([],set(range(1,len(NEIGHBORS))),set(),report)
        maximaleCliques = report.cliques
        DefinitieveMaximaleCliques += maximaleCliques
        if len(DefinitieveMaximaleCliques)%1000 <= 20:
            print len(DefinitieveMaximaleCliques)
    return DefinitieveMaximaleCliques






main()






