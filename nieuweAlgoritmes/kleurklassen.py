

def verdeelInKleurklassen(graaf):
    kleurklassen = [[1]]
    for i in range(2, len(graaf)):
        for i2 in range(len(kleurklassen)):
            if set(graaf[i]).intersection(set(kleurklassen[i2])) != set():
                if i2 == len(kleurklassen) - 1:
                    kleurklassen.append([i])
            else:
                kleurklassen[i2].append(i)
                break
    return kleurklassen
