__author__ = 'Michael'

import random
import numpy
import timeit

## verdeelInKleurklassen verdeelt de knooppunten van de graaf over kleurklassen. Elke kleurklasse bevat
# knooppunten die onderling geen buren zijn. De opvulling gebeurt vanuit het knooppunt perspectief.
# Dit wil zeggen dat voor elk knooppunt een geschikte kleurklasse wordt gezocht en indien die niet bestaat,
# wordt een nieuwe kleurklasse gemaakt.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# return kleurklassen: een lijst met kleurklassen die elk zijn voorgesteld door een lijst van knooppunten

def verdeelInKleurklassen(graaf):
    kleurklassen = []
    for i in range(0,len(graaf)):
        geplaatst = False
        kleurklasse = 0
        while geplaatst != True:
            geplaatst = True
            if kleurklasse == len(kleurklassen):
                kleurklassen.append([i])
            else:
                for knooppunt in graaf[i]:
                    if knooppunt in kleurklassen[kleurklasse]:
                        geplaatst = False
                if geplaatst:
                    kleurklassen[kleurklasse].append(i)
                kleurklasse += 1
    return kleurklassen


## bepaalGewogenKansenKleurklassen bepaalt de kans waarmee elke kleurklasse wordt geselecteerd om later een knooppunt
# uit te kiezen. Deze kans is evenredig met de som van de graden van alle knooppunten binnen de kleurklasse
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklassen: de lijst met de gebruikte kleurklassen van toepassing op de graaf
# return kansenKleurklassen: een lijst met de kans voor elke kleurklasse

def bepaalGewogenKansenKleurklassen(graaf,kleurklassen):
    gradenKleurklassen = len(kleurklassen)*[0]
    for i in range(0,len(kleurklassen)):
        for knooppunt in kleurklassen[i]:
            gradenKleurklassen[i] += len(graaf[knooppunt])
    somGraden = 0
    for graad in gradenKleurklassen:
        somGraden += graad
    kansenKleurklassen = len(kleurklassen)*[0]
    for kleurklasse in range(len(kleurklassen)):
        kansenKleurklassen[kleurklasse] = gradenKleurklassen[kleurklasse]/somGraden
    return kansenKleurklassen

## kiesKleurklasseMetGewogenKansen kiest een kleurklasse op basis van de som van de graden
# van de knooppunten in deze kleurklassen
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# kansenKleurklassen: de lijst met de kansverdeling voor elk van de kleurklassen
# return kleurklasse: het nummer van de gekozen kleurklasse

def kiesKleurklasseMetGewogenKansen(kleurklassen,kansenKleurklassen):
    kleuren = range(0,len(kleurklassen))
    kleurklasse = numpy.random.choice(kleuren,p = kansenKleurklassen)
    return kleurklasse

## bepaalGewogenKansenKnooppuntenVoorEenKleurklasse bepaalt de kans waarmee een knooppunt gekozen wordt
# uit een gegeven kleurklasse. De kanse is evenredig met het aantal buren van het knooppunt.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklasse: de lijst met de gekozen kleurklasse
# return kansenKnooppunten: een lijst met de kansen van de knooppunten in de kleurklasse

def bepaalGewogenKansenKnooppuntenVoorEenKleurklasse(graaf,kleurklasse):
    gradenKnooppunten = len(kleurklasse)*[0]
    i = 0
    for knooppunt in kleurklasse:
        gradenKnooppunten[i] += len(graaf[knooppunt])
        i += 1
    somGraden = 0
    for graad in gradenKnooppunten:
        somGraden += graad
    kansenKnooppunten = len(kleurklasse)*[0]
    for i in range(0,len(kleurklasse)):
        kansenKnooppunten[i] = gradenKnooppunten[i]/somGraden
    return kansenKnooppunten

## bepaalGewogenKansenKnooppuntenBinnenElkeKleurklasse bepaalt de kansen voor elk knooppunt binnen elke kleurklasse.
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# return kansenKnooppuntenPerKleurklasse: een lijst met per kleurklasse een lijst met de kansen op de knooppunten

def bepaalGewogenKansenKnooppuntenBinnenElkeKleurklasse(graaf,kleurklassen):
    kansenKnooppuntenPerKleurklasse = []
    for kleurklasse in kleurklassen:
        kansenKnooppuntenPerKleurklasse.append(bepaalGewogenKansenKnooppuntenVoorEenKleurklasse(graaf,kleurklasse))
    return kansenKnooppuntenPerKleurklasse

## kiesKnooppuntUitKleurklasse kiest een knooppunt uit een kleurklasse met een kans evenredig aan zijn aantal buren.
# kleurklasse: de lijst met de gekozen kleurklasse
# kansenKnooppunten: de lijst met de kansen van de knooppunten in de kleurklasse
# return knooppunt: het gekozen knooppunt

def kiesKnooppuntUitKleurklasse(kleurklasse,kansenKnooppunten):
    knooppunt = numpy.random.choice(kleurklasse,p = kansenKnooppunten)
    return knooppunt

## genereerEersteOplossing bepaalt de eerste mogelijke oplossing.
# aantalVerzamelingen: het gewenste aantal mogelijk cliques in de startoplossing
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# kansenAlleKnooppunten: de lijst met per kleurklasse de kans voor elk knooppunt
# return mogelijkeCliques: de eerste oplossing bestaande uit een lijst van lijsten van knooppunten


def genereerEersteOplossing(aantalVerzamelingen,kleurklassen,kansenAlleKnooppunten):
    mogelijkeCliques = []
    for i in range(0,aantalVerzamelingen):
        for kleur in range(0,len(kleurklassen)):
            if kleur == 0:
                mogelijkeCliques.append([kiesKnooppuntUitKleurklasse(kleurklassen[kleur],kansenAlleKnooppunten[kleur])])
            else:
                mogelijkeCliques[i].append(kiesKnooppuntUitKleurklasse(kleurklassen[kleur],kansenAlleKnooppunten[kleur]))
    return mogelijkeCliques

## cliqueTest gaat na of een gegeven verzameling van knooppunten een clique is
# verzameling: een lijst met de te controleren verzameling van knooppunten
# graaf: de te onderzoeken graaf (lijst van lijsten)
# return clique: een boolean die True geeft als de verzameling een clique vormt

def cliqueTest(verzameling,graaf):
    clique = True
    knoop = 0
    testknoop = 1
    while clique and knoop < len(verzameling)-1:
        if verzameling[testknoop] in graaf[verzameling[knoop]]:
            if testknoop < len(verzameling)-1:
                testknoop += 1
            else:
                knoop += 1
                testknoop = knoop + 1
        else:
            clique = False
    return clique

## maximaleCliqueTest gaat na of een gegeven clique maximaal is
# clique: de te controleren clique in de vorm van een lijst
# graaf: de te onderzoeken graaf (lijst van lijsten)
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# return maximaleClique: een boolean die True geeft als de clique maximaal is

def maximaleCliqueTest(clique,graaf,kleurklassen):
    if len(clique) == len(kleurklassen):
        maximaleClique = True
    else:
        gemeenschappelijkeBuren = set(graaf[clique[0]])
        for i in range(1,len(clique)):
            gemeenschappelijkeBuren = gemeenschappelijkeBuren.intersection(set(graaf[clique[i]]))
        if gemeenschappelijkeBuren == set():
            maximaleClique = True
        else:
            maximaleClique = False
    return maximaleClique

## nieuweKandidaatClique genereert een nieuwe mogelijke clique met uit elke kleurklasse een knooppunt
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# kansenAlleKnooppunten: de lijst met per kleurklasse de kans voor elk knooppunt
# return nieuweMogelijkeClique: een lijst met een nieuwe verzameling van knooppunten

def nieuweKandidaatClique(kleurklassen,kansenAlleKnooppunten):
    nieuweMogelijkeClique = []
    for kleur in range(0,len(kleurklassen)):
            nieuweMogelijkeClique.append(kiesKnooppuntUitKleurklasse(kleurklassen[kleur],kansenAlleKnooppunten[kleur]))
    return nieuweMogelijkeClique

## vindNieuwKnooppunt genereert een nieuw knooppunt om aan een clique toe te voegen.
# Dit knooppunt komt uit een andere kleurklasse dan de kleurklassen van de knooppunten in de clique. De kans om een bepaald
# knooppunt te kiezen stijgt met de som van het aantal buren van alle knooppunten in zijn kleurklasse en binnen de kleurklasse
# met zijn eigen aantal buren.
# clique: een lijst die de uit te breiden clique bevat
# kleurklassen: de lijst van de kleurklassen van toepassing op de graaf
# kansenKleurklassen: de lijst met de kansverdeling voor elk van de kleurklassen
# kansenAlleKnooppunten: de lijst met per kleurklasse de kans voor elk knooppunt
# return nieuweKnooppunt: het uitgekozen nieuwe knooppunt

def vindNieuwKnooppunt(clique,kleurklassen,kansenKleurklassen,kansenAlleKnooppunten):
    kleuren = []
    for knooppunt in clique:
        gevonden = False
        i = 0
        while gevonden != True:
            if knooppunt in kleurklassen[i]:
                kleuren.append(i)
                gevonden = True
            i += 1
    kleur = kiesKleurklasseMetGewogenKansen(kleurklassen,kansenKleurklassen)
    kleurFout = kleur in kleuren
    while kleurFout == True:
        kleur = kiesKleurklasseMetGewogenKansen(kleurklassen,kansenKleurklassen)
        if kleur not in kleuren:
            kleurFout = False
    nieuweKnooppunt = kiesKnooppuntUitKleurklasse(kleurklassen[kleur],kansenAlleKnooppunten[kleur])
    return nieuweKnooppunt


## genereerMaximaleCliques genereert een bepaald aantal maximale cliques in een graaf
# return maximaleCliques: een lijst van gevonde maximale cliques die zelf worden voorgesteld door lijsten


def genereerMaximaleCliques(graaf,maxAantalIteraties):
    initieelAantalKandidaatCliques = 10
    kleurklassen = verdeelInKleurklassen(graaf)
    kansenKleurklassen = bepaalGewogenKansenKleurklassen(graaf,kleurklassen)
    kansenAlleKnooppuntenBinnenKleurklasse = bepaalGewogenKansenKnooppuntenBinnenElkeKleurklasse(graaf,kleurklassen)
    huidigeOplossing = genereerEersteOplossing(initieelAantalKandidaatCliques,kleurklassen,kansenAlleKnooppuntenBinnenKleurklasse)
    besteOplossing = huidigeOplossing
    maximaleCliques = []
    nieuweOplossing = huidigeOplossing
    n = 0
    tijd0 = 0
    tijd1 = 0
    tijd2 = 0
    tijd3 = 0

    while n < maxAantalIteraties:
        aantalKandidaatCliques = len(huidigeOplossing)
        gekozenKandidaatClique = random.randint(0,aantalKandidaatCliques-1)
        start0 = timeit.default_timer()
        isClique = cliqueTest(nieuweOplossing[gekozenKandidaatClique],graaf)
        stop0 = timeit.default_timer()
        tijd0 += (stop0 - start0)
        if isClique != True:
            teVerwijderenKnoop = random.randint(0,len(nieuweOplossing[gekozenKandidaatClique])-1)
            nieuweOplossing[gekozenKandidaatClique].pop(teVerwijderenKnoop)
        else:
            start1 = timeit.default_timer()
            isCliqueMaximaal = maximaleCliqueTest(huidigeOplossing[gekozenKandidaatClique],graaf,kleurklassen)
            stop1 = timeit.default_timer()
            tijd1 += (stop1 - start1)
            if isCliqueMaximaal == True:
                maximaleClique = nieuweOplossing.pop(gekozenKandidaatClique)
                count = 0
                duplicaat = False
                while duplicaat == False and count < len(maximaleCliques):
                    if set(maximaleClique) == set(maximaleCliques[count]):
                        duplicaat = True
                    else:
                        count += 1
                if duplicaat == False:
                    maximaleCliques.append(maximaleClique)
                start2 = timeit.default_timer()
                nieuweOplossing.append(nieuweKandidaatClique(kleurklassen,kansenAlleKnooppuntenBinnenKleurklasse))
                stop2 = timeit.default_timer()
                tijd2 += (stop2 - start2)
            else:
                start3 = timeit.default_timer()
                nieuweKnooppunt = vindNieuwKnooppunt(huidigeOplossing[gekozenKandidaatClique],kleurklassen,kansenKleurklassen,kansenAlleKnooppuntenBinnenKleurklasse)
                nieuweOplossing[gekozenKandidaatClique].append(nieuweKnooppunt)
                stop3 = timeit.default_timer()
                tijd3 += (stop3 - start3)
        n += 1
    print(tijd0,tijd1,tijd2,tijd3)
    return maximaleCliques










