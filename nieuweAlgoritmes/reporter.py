# coding: utf-8

class Reporter(object):
    def __init__(self, name,bestand):
        self.name = name
        self.cnt = 0
        self.cliques = []
        self.bestand = bestand

    def inc_count(self):
        self.cnt += 1
 
    def record(self, clique):
        self.cliques.append(clique)
 
    def print_report(self):
        print self.name
        print '%d recursive calls' % self.cnt
        for i, clique in enumerate(self.cliques):
           print '%d: %s' % (i, clique)
        print

    def wegschrijven(self):
        file = open(str(self.bestand),'a')
        file.write(str(self.cliques[-1]))
        file.write('\n')
        file.flush()
        file.close()